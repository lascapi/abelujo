Changelog starting from v0.5 (2018/09/18)


# Dev

### 2018/09

- a list/basket can be linked to a supplier, so than further actions
  (apply to the stock, receive a parcel,…) can set this supplier for
  all the cards.
